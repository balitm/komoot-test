//
//  UITableView+Extension.swift
//  wizz-air
//
//  Created by Balázs Kilvády on 1/20/17.
//  Copyright © 2017 bcsconsulting. All rights reserved.
//

import UIKit

protocol ReuseID {
    static var kReuseID: String { get }
}

extension ReuseID {
    static var kReuseID: String {
        return String(describing: Self.self)
    }
}

extension UITableView {
    func dequeue<Cell>(type: Cell.Type, for indexPath: IndexPath) -> Cell where Cell: ReuseID, Cell: UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: Cell.kReuseID, for: indexPath) as! Cell
        return cell
    }

    func dequeue<Cell>(type: Cell.Type) -> Cell where Cell: ReuseID, Cell: UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: Cell.kReuseID) as! Cell
        return cell
    }
}
