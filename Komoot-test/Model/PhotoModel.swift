//
//  Picture.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import Foundation

struct PhotoModel : Decodable {
    let farm: Int
    let id: String
    let secret: String
    let server: String

    var urlString: String {
        let str = "https://farm" + String(farm) + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_c.jpg"
        return str
    }
}
