//
//  ImageManager.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import UIKit

private let _kImagesToDrop = 5

struct ImageManager: Sequence {
    typealias Iterator = Array<UIImage>.Iterator

    private var _images = [UIImage]()

    /// Returns an iterator over the elements of this sequence.
    public func makeIterator() -> IndexingIterator<Array<UIImage>> {
        return _images.makeIterator()
    }

    mutating func append(_ image: UIImage) {
        _images.insert(image, at: 0)
    }

    mutating func drop() {
        _images = Array(_images.dropLast(_kImagesToDrop))
    }

    var count: Int { return _images.count }

    subscript(index: Int) -> UIImage {
        return _images[index]
    }
}
