//
//  API.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

private let _kKey = "88306bf875bfbd02fee3bb810b71da50"
private let _kSecret = "2db35f6718ce188d"
private let _kBaseURLString = "https://api.flickr.com/services/rest/"
private let _parameters: [String : Any] = [
    "method" : "flickr.photos.search",
    "api_key" : _kKey,
    "format" : "json",
    "accuracy" : "11",
    "per_page" : 1,
    "pages" : 1,
    "nojsoncallback" : 1]

private let _kImageCacheDir = "Image/temp.jpg"

final class API {
    typealias DataResponseParser = (_ request: URLRequest?, _ response: HTTPURLResponse?, _ data: Data?, _ error: Error?) -> Alamofire.Result<PhotoModel>
    typealias DownloadResponseParser = (_ request: URLRequest?, _ response: HTTPURLResponse?, _ url: URL?, _ error: Error?) -> Alamofire.Result<UIImage>

    private static var _cacheURL: URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let cacheURL = documentsURL.appendingPathComponent(_kImageCacheDir)

        do {
            try FileManager.default.createDirectory(at: cacheURL, withIntermediateDirectories: false)
        } catch {
        }
        return cacheURL
    }

    static private let _toProvider: DownloadRequest.DownloadFileDestination = { url, response in
        let fileURL = _cacheURL
        return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }

    // TODO: Do we need locking between getImage() and emptyCache()? Just for sure:
    static private let _mutex = NSLock()

    static func getImage(location: CLLocation, completion: @escaping (UIImage) -> Void) {
        guard let url = URL(string: _kBaseURLString) else { fatalError("Invalid base url: \(_kBaseURLString)") }

        var params = _parameters
        params["lat"] = location.coordinate.latitude
        params["lon"] = location.coordinate.longitude

        let headers = ["Accept": "application/json",
                       "Accept-Encoding": "gzip"]

        API._mutex.lock()
        Alamofire.request(url, parameters: params, headers: headers)
            .validate(contentType: ["application/json"])
            .response(responseSerializer: _dataResponseSerializer) { response in
                // DLog("Resp: ", String(data: response.data!, encoding: .utf8) ?? "")

                switch response.result {
                case .failure(let error):
                    DLog("# Getting photo list failed: ", error)
                    API._mutex.unlock()
                case .success(let photo):
                    let download = Alamofire.download(photo.urlString,
                                                      parameters: params,
                                                      to: API._toProvider)
                    DLog("Downloading.")
                    download.response(responseSerializer: API._downloadResponseSerializer) { response in
                        defer { API._mutex.unlock() }

                        guard let image = response.value else { return }

                        completion(image)
                    }
                }
            }
    }

    static func emptyCache() {
        DispatchQueue.global(qos: .background).async {
            API._mutex.lock(); defer { API._mutex.unlock() }

            URLCache.shared.removeAllCachedResponses()
        }
    }

    // MARK: Response parsers

    private static let _dataResponeParser: DataResponseParser = { request, response, data, error in
        guard let data = data else { return .failure(AFError.responseSerializationFailed(reason: .inputDataNil)) }

        let decoder = JSONDecoder()
        do {
            let list = try decoder.decode(PhotoListModel.self, from: data)
            guard let photo = list.photos.photo.last else {
                DLog("# No photo")
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return .success(photo)
        } catch let error {
            DLog("Caught error: ", error)
            return .failure(error)
        }
    }

    private static let _dataResponseSerializer = DataResponseSerializer<PhotoModel> { urlRequest, response, data, error in
        var result: Alamofire.Result<PhotoModel>

        if let error = error {
            result = .failure(error)
        } else {
            result = _dataResponeParser(urlRequest, response, data, error)
        }

        return result
    }

    private static var _downloadResponseSerializer = DownloadResponseSerializer<UIImage> { urlRequest, response, url, error in
        var result: Alamofire.Result<UIImage>

        if let error = error {
            DLog("# Downloading failed with ", error)
            result = .failure(AFError.responseSerializationFailed(reason: .inputFileNil))
        } else {
            result = _downloadResponseParser(urlRequest, response, url, error)
            if let model = result.value {
                result = .success(model)
            } else {
                result = .failure(AFError.responseSerializationFailed(reason: .inputFileNil))
            }
        }

        return result
    }

    private static let _downloadResponseParser: DownloadResponseParser = { request, response, url, error in
        guard error == nil else { return .failure(error!) }

        guard let fileURL = url else {
            return .failure(AFError.responseSerializationFailed(reason: .inputFileNil))
        }

        if let data = try? Data(contentsOf: fileURL)
            , let image = UIImage(data: data) {
            return .success(image)
        }
        return .failure(AFError.responseSerializationFailed(reason: .inputFileReadFailed(at: fileURL)))
    }
}
