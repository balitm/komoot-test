//
//  LocationManager.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications
import MapKit

private let _kMetersThreshold: CLLocationDistance = 100
private let _kSecondsThreshold: TimeInterval = 60

final class LocationManager: NSObject {
    private let _locationManager = CLLocationManager()
    private let _locationChanged: (CLLocation) -> Void
    private var _lastTime = Date.distantPast
    private var _lastLocation = CLLocationCoordinate2D()

    init(locationChanged: @escaping (CLLocation) -> Void) {
        _locationChanged = locationChanged
        super.init()
    }

    func start() {
        guard CLLocationManager.authorizationStatus() != .denied else { return }

        let notificationCenter = UNUserNotificationCenter.current()

        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.pausesLocationUpdatesAutomatically = false
        _locationManager.allowsBackgroundLocationUpdates = true
        _locationManager.requestAlwaysAuthorization()
        _locationManager.delegate = self
        notificationCenter.delegate = self
        notificationCenter.requestAuthorization(options: [.alert, .badge]) { granted, error in
            DLog("NotificationCenter authorization granted: ", granted, ", ", error?.localizedDescription ?? "no error")
        }
        _locationManager.startUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    private func _distance(_ lhs: CLLocationCoordinate2D, _ rhs: CLLocationCoordinate2D) -> CLLocationDistance {
        let point0 = MKMapPointForCoordinate(lhs)
        let point1 = MKMapPointForCoordinate(rhs)
        return MKMetersBetweenMapPoints(point0, point1)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last
            , _distance(location.coordinate, _lastLocation) >= _kMetersThreshold else { return }
        guard location.timestamp > _lastTime.addingTimeInterval(_kSecondsThreshold)
            , location.timestamp.addingTimeInterval(_kSecondsThreshold) > Date() else { return }

        DLog("New location: ", location.coordinate, " at ", location.timestamp)
        _lastLocation = location.coordinate
        _lastTime = location.timestamp
        _locationChanged(location)
        _scheduleNotification(location.coordinate)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let clError = CLError(_nsError: error as NSError)
        switch clError.code {
        case .denied:
            DLog("Core location denied.")
        default:
            DLog("Core location failure: \(error.localizedDescription)")
        }
    }
}

extension LocationManager: UNUserNotificationCenterDelegate {
    private func _scheduleNotification(_ location: CLLocationCoordinate2D) {
        let center = UNUserNotificationCenter.current()

        center.removeAllPendingNotificationRequests()

        let content = UNMutableNotificationContent()
        content.title = "Next zone"
        content.body = "Start requesting photo"
        content.categoryIdentifier = "alarm"

        // Trigger when out of a location.
        let region = CLCircularRegion(center: location, radius: 100, identifier: UUID().uuidString) // radius in meters
        region.notifyOnEntry = false
        region.notifyOnExit = true
        let trigger = UNLocationNotificationTrigger(region: region, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.removeAllDeliveredNotifications()
        center.add(request)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
