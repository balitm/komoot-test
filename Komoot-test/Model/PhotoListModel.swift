//
//  PictureListModel.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import Foundation

struct Photos: Decodable {
    let photo: [PhotoModel]
}

struct PhotoListModel: Decodable {
    let photos: Photos
    let stat: String
}
