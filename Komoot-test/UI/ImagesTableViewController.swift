//
//  ImagesTableViewController.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import UIKit

final class ImagesTableViewController: UITableViewController {

    private var _locationManager: LocationManager!
    private var _images = ImageManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.hidesBackButton = true
        _locationManager = LocationManager { [weak self] location in
            guard let `self` = self else { return }

            DLog("Recved loc: ", location)
            API.getImage(location: location) { [weak self] image in
                guard let `self` = self else { return }

                DLog("Inserting next image, current count: ", self._images.count)
                self._images.append(image)
                self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }
        }
        _locationManager.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        _images.drop()
        API.emptyCache()
        tableView.reloadData()
    }
}

// MARK: - Table view data source

extension ImagesTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _images.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(type: PhotoTableViewCell.self, for: indexPath)

        cell.photo = _images[indexPath.row]
        return cell
    }
}
