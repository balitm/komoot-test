//
//  ImageTableViewCell.swift
//  Komoot-test
//
//  Created by Balázs Kilvády on 7/21/18.
//  Copyright © 2018 Balázs Kilvády. All rights reserved.
//

import UIKit

final class PhotoTableViewCell: UITableViewCell, ReuseID {

    @IBOutlet weak var photoView: UIImageView!

    var photo: UIImage? {
        get { return photoView.image }
        set { photoView.image = newValue }
    }
}
